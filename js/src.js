var width = window.innerWidth *0.8;
var height = window.innerHeight -5;
var paint;
var clickX = new Array();
var clickY = new Array();
var clickDrag = new Array();
var clickColor = new Array();
var clickSize = new Array();
var currentColor, currentSize, dragging = false;
var img = document.createElement("img");

function startApp () {
	initControls();
	initApp();
}

function initApp(){
	console.log(img.src);

	canvas = document.getElementById("paintApp");

	if(typeof G_vmlCanvasManager != 'undefined'){
		canvas = G_vmlCanvasManager.initElement(canvas);
	}

	context = canvas.getContext("2d");
	canvas.width = width;
	canvas.height = height;

	//Listeners
	canvas.onmousedown = function(e){
		console.log(e);
		console.log(this);
		var context = (e.toElement) ? e.toElement.getContext("2d") : e.target.getContext("2d");
		paint = true;
		addClick(e.x - this.offsetLeft, e.y -this.offsetTop);
		redraw(context);

	}
	canvas.onmousemove = function(e){
		var px = e.pageX - this.offsetLeft;
		var py = e.pageY - this.offsetTop;

		if(paint){
			addClick(px,py,true);
			redraw(context);
		}
	}
	canvas.onmouseup = function(e){paint = false;}
	canvas.onmouseleave = function(e){paint = false;}

	// DRAG & DROP API
	canvas.addEventListener("dragover", function (e) {
		e.preventDefault();
	}, false);

	canvas.addEventListener("drop", function (e) {
		var files = e.dataTransfer.files;
		if(files.length > 0){
			var file = files[0];
			if(typeof FileReader !== "undefined"){
				console.log(file.type.indexOf("image"));
				if(file.type.indexOf("image") != -1){
					var reader = new FileReader();
					reader.onload= function(e){
						img.src = e.target.result;
						console.log(img.src);
					};
					reader.readAsDataURL(file);
				}
			}
		}
		e.preventDefault();
	}, false);

	//IMAGE HANDLER
	img.addEventListener("load", function(){
		clickX = [];
		clickY = [];
		clickDrag = [];
		clickColor = [];
		clickSize = [];
		canvas.width = img.width;
		canvas.height = img.height;
		context.drawImage(img,0,0);
	},false);
}

function addClick(x,y,dragging){
	clickX.push(x);
	clickY.push(y);
	clickDrag.push(dragging);
	clickColor.push(currentColor);
	clickSize.push(currentSize);
}

function redraw(context){
	context.clearRect(0,0, context.canvas.width, context.canvas.height);

	context.lineJoin = "round";

	if(img.src != null){
		context.drawImage(img,0,0);
	}

	for (var i = 0; i < clickX.length; i++) {
		context.beginPath();
		if(clickDrag[i] && i){
			context.moveTo(clickX[i-1], clickY[i-1]);
		}else{
			context.moveTo(clickX[i]-1, clickY[i]);
		}

		context.lineTo(clickX[i],clickY[i]);
		context.closePath();
		context.strokeStyle = clickColor[i];
		context.lineWidth = clickSize[i];
		context.stroke();
	};
}

function initControls(){

	//captura de elementos de control
	var brushSizeCtrl = document.getElementById("brushSize");
	var brushColorCtrl = document.getElementById("brushColor");
	var saveCtrl = document.getElementById("save");
	var loadImageCtrl = document.getElementById("loadImage");

	//incializar variables de control
	currentSize = brushSizeCtrl.value;
	currentColor = brushColorCtrl.value;

	//Listeners
	brushSizeCtrl.onchange = function(e){
		currentSize = this.value;
	}

	brushColorCtrl.onchange = function(e){
		currentColor = this.value;
	}

	saveCtrl.onclick = function(e){
		window.open(canvas.toDataURL("image/png"),"_BLANK");
		e.preventDefault();
	}

	loadImageCtrl.onclick = function(e){
		img.src = "./assets/pony.png";
	}

	//Borrador
	document.getElementById("borrador").onclick = function(e){
		if(this.innerHTML == "Borrador"){
			currentColor = "#ffffff";
			this.innerHTML = "Pincel";
		}else{
			currentColor = brushColorCtrl.value;
			this.innerHTML = "Borrador";
		}
	}

	//Reset
	document.getElementById("reset").onclick = function(e){
		clickX = [];
		clickY = [];
		clickDrag = [];
		clickColor = [];
		clickSize = [];
		img.src = '';
		canvas.width = width;
		canvas.height = height;
		redraw(context);
	}
}